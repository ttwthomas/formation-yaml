---
theme: default
paginate: true
marp: true
class: invert
--- 
<style >
.hljs-string {color: SandyBrown;}
.hljs-attr {color: lightBlue;}
</style>

# YAML
Yet another markup language ...
Encore un autre language de balisage ...

---
# Role Ansible
```yaml
---
- name: Update web servers
  hosts: webservers
  remote_user: root

  tasks:
  - name: Ensure apache is at the latest version
    ansible.builtin.yum:
      name: httpd
      state: latest
  - name: Write the apache config file
    ansible.builtin.template:
      src: /srv/httpd.j2
      dest: /etc/httpd.conf

- name: Update db servers
  hosts: databases
  remote_user: root

  tasks:
  - name: Ensure postgresql is at the latest version
    ansible.builtin.yum:
      name: postgresql
      state: latest
  - name: Ensure that postgresql is started
    ansible.builtin.service:
      name: postgresql
      state: started
```
---
# Docker-compose
```yaml
version: '2.2'
services:
  nodetemplate: &node
    image: docker.elastic.co/elasticsearch/elasticsearch:7.14.0
    container_name: es01
    environment:
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es02,es03
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data01:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - elastic

    es01:
    << : *node
    container_name: es01
    es02:
    << : *node
    container_name: es02

volumes:
  data01:
    driver: local
  data02:
    driver: local

networks:
  elastic:
    driver: bridge
```
---
# Gitlab CI
```yaml
image: brambaud/marp-cli-ci

pages:
  script:
    - node /home/marp/.cli/marp-cli.js *.md
    - mkdir public
    - cp -r *.html public
    - cp -r images/ public
  artifacts:
    paths:
      - public
  only:
    - master

```
---

|YAML           |JSON                   |
|-              |-                      |
|clean  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|moche                  |
|facile         | facile de se tromper  |
|smart          | bête |
|humain         |machine|
|lent           |rapide|

<!-- json plus rapide a parser -->

---
![bg contain](images/benchmark.png)
<!-- 10k object -->

---
# Data types

---
# Scalars (string et chiffres)
```yaml
---
maChaine: "ma chaine de caractere"
prenom: Thomas
age: 32
```

<!-- 
commence toujours avec ---
avec ou sans guillemets
string ou int, autodetect
-->

---
# Sequences (tableaux, liste)
```yaml
---
maListe:
- premier
- deuxieme
- troisieme
```
<!-- ordoné -->

---
# Mappings (objets)
```yaml
---
cocadmin: 
    prenom: Thomas
    age: 32
    tableau:
    - premier
    - deuxieme
    - troisieme
    - quatrieme:
        maClef: Valeure
```


---
# Superset de JSON
```yaml
monTableau: [premier, deuxieme, "troisieme"]
monObjet: { clef: valeur, autreClef: autreValeur}
```
équivaut:
```json
{
    "monTableau": ["premier", "deuxieme", "troisieme"],
    "monObjet": { "clef": "valeur", "autreClef": "autreValeur"}
}

```

<!-- json sans guillement ni virgule a la fin -->

---
# Commentaires
```yaml
# on peu mettre des commentaire
cocadmin: 
    prenom: Thomas
    age: 32
    # où on veut !
    tableau:
    - premier
    - deuxieme
    - troisieme # même la !
```
<!-- impossible en json -->

---
# |
```yaml
deployement:
    script: |
        scp deployment.zip admin@cours.cocadmin.com:/home/admin
        ssh admin@cours.cocadmin.com "unzip /home/admin -d /var/www/html"
        ssh admin@cours.cocadmin.com "service restart apache2"
        sendmail thomas@cocadmin.com  < /tmp/email.txt
```
équivaut:
```yaml
deployement:
    script: 'scp deployment.zip admin@cours.cocadmin.com:/home/admin \n ssh admin@cours.cocadmin.com "unzip /home/admin -d /var/www/html" \n ssh admin@cours.cocadmin.com "service restart apache2" \n sendmail thomas@cocadmin.com  < /tmp/email.txt'
```
---
# > 

```yaml
deployement:
    script: >
      ssh -oStrictHostKeyChecking=no 
      ec2-user@$ENV.cocadmin.tk 
      tar -xvzf /home/ec2-user/deployment_package.tar.gz 
      -C /code
```
équivaut
```yaml
deployment:
  script: ssh -oStrictHostKeyChecking=no ec2-user@$ENV.cocadmin.tk tar -xvzf /home/ec2-user/deployment_package.tar.gz -C /code

```

<!-- remplace les newline par des espace -->

---
# Variables
```yaml
monPassword: ChristopheLeBG &pass
pass: *pass
```
équivaut
```yaml
monPassword: ChristopheLeBG 
pass: ChristopheLeBG
```

---
# Encres

```yaml
deployement-template: &deploy
    variables: 
        PATH: /var/www/hmtl
    script: |
    scp deployment.zip admin@cours.cocadmin.com:/home/admin
    ssh admin@cours.cocadmin.com "unzip /home/admin -d $PATH"
    ssh admin@cours.cocadmin.com "service restart apache2"
    sendmail thomas@cocadmin.com  < /tmp/email.txt

prod:
    <<: &deploy
    environment: PROD
dev:
    <<: &deploy
    environment: DEV
    variables: 
      PATH: /var/www/dev

```
---
équivaut
```yaml
prod: 
    variables: 
        PATH: /var/www/hmtl
    script: |
      scp deployment.zip admin@cours.cocadmin.com:/home/admin
      ssh admin@cours.cocadmin.com "unzip /home/admin -d $PATH"
      ssh admin@cours.cocadmin.com "service restart apache2"
      sendmail thomas@cocadmin.com  < /tmp/email.txt
    environment: PROD

dev: 
    variables: 
        PATH: /var/www/dev
    script: |
      scp deployment.zip admin@cours.cocadmin.com:/home/admin
      ssh admin@cours.cocadmin.com "unzip /home/admin -d $PATH"
      ssh admin@cours.cocadmin.com "service restart apache2"
      sendmail thomas@cocadmin.com  < /tmp/email.txt
    environment: dev

```
<!-- imagine avec plus d'environement -->

